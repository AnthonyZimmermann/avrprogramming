#!/bin/bash

optim=-O1

set -e
#avr-gcc -fshort-enums $optim -Wall -mmcu=atmega8 main.c -S #-0s
avr-gcc -fshort-enums $optim -Wall -mmcu=atmega8 main.c    #-0s
#avr-gcc -Wall -Os -mmcu=atmega8 main.c utils.c
avr-size --mcu=atmega8 -C a.out
#avr-gcc -Wl,-Map,app.map
avr-objcopy -j .text -j .data -O ihex a.out a.hex
sudo avrdude -p atmega8 -c linuxspi -P /dev/spidev0.0 -v -U flash:w:a.hex:i -F
