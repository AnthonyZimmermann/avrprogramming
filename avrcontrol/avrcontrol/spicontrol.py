import time
import spidev

COMM_DUMMY                          = 0x00 # should be different from COMM_OK
COMM_RESPONSE_STRETCH               = 0xF9
COMM_RESPONSE_START                 = 0xFE
COMM_RESPONSE_EMPTY                 = 0xFA
COMM_OK                             = 0xF0
COMM_ERR                            = 0xF3

COMMAND_NOP                         = 251
COMMAND_GET_COMMAND_BUFFER_SIZE     = 252
COMMAND_GET_RESPONSE_BUFFER_SIZE    = 253
COMMAND_GET_VERSION                 = 254
# only if mc was compiled with: #define DEBUG
COMMAND_GET_DEBUG                   = 255

VERSION = 5

class SpiControlError(Exception):
    def __init__(self, command=None, errors=[], message=None):
        self.command = command
        self.attempts = len(errors)
        self.errors = dict()
        for e in errors:
            e_name = e.__class__.__name__
            self.errors[e_name] = self.errors.get(e_name,0)+1

        self.message = "Failed communication." if message is None else message
        if self.command is not None:
            self.message += "; Command: {}".format(self.command)
        if self.attempts is not None:
            self.message += "; Attempts: {}".format(self.attempts)
        for k,i in self.errors.items():
            self.message += "; {}: {}".format(k,i)
        super().__init__(self.message)

class VersionReadoutError(SpiControlError):
    pass

class RealtimeViolation(SpiControlError):
    pass

class ResetError(SpiControlError):
    pass

class CommandTransError(SpiControlError):
    pass

class ResponseTransError(SpiControlError):
    pass

class ResponseTimeout(SpiControlError):
    pass

class ResponseStretchError(SpiControlError):
    pass

class SpiControl(object):
    # COMM_RESET_TIME = COMM_RESET_DEFER_TIMEOUT_US in microcontroller
    # these constants must match
    # The time defines the idle time in the microcontroller spi communication that
    # leads to a communication reset
    # A reasonable value depends on the COMM_SPEED_HZ and on the master spi driver speed
    # in terms of fetching the response bytes one byte after another
    COMM_RESET_TIME = 10e-3
    COMM_COMMAND_BUFFER_SIZE = 16
    COMM_SPEED_HZ = 8000
    COMM_BYTE_TRANSFER_TIME = 8/COMM_SPEED_HZ

    def __init__(self, bus=0, dev=0, mode=0b00):
        self.bus = bus
        self.dev = dev
        self.max_speed_hz = SpiControl.COMM_SPEED_HZ
        self.mode = mode
        self.spi = spidev.SpiDev()
        self.is_open = False
        self.last_xfer_time = time.time() # initial value must be float

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, exception_type, exception_value, exception_traceback):
        self.close()

    def open(self):
        if self.is_open:
            return
        self.is_open = True
        self.spi.open(self.bus, self.dev)
        self.spi.max_speed_hz = self.max_speed_hz
        self.spi.mode = self.mode
        # setting last_xfer_time to "now" ensures that mu resets communication
        # within resetMUCommunication()
        self.last_xfer_time = time.time()
        self.mu_version = None

        # try to read a value twice to validate a workin communication
        attempts = 10
        for i in range(1,attempts+1):
            try:
                self.mu_version = self.getVersion()
                if self.mu_version == self.getVersion():
                    break
            except:
                pass
            if i == attempts:
                raise VersionReadoutError(message="Initial communication failed.")
        if self.mu_version != VERSION:
            raise VersionReadoutError(message="Incompatible MU version.")

    def close(self):
        self.spi.close()
        self.is_open = False

    @staticmethod
    def encode(value, num_bytes, decimal_place=0):
        val = int(value*(2**decimal_place))
        out = []
        for i in range(num_bytes):
            out.append(val&0xFF)
            val = val >> 8
        return out

    @staticmethod
    def decode(byte_array, decimal_place=0):
        out = 0
        for n in reversed(byte_array):
            out = out << 8
            out += n
        if decimal_place == 0:
            return out
        return out/(2**decimal_place)

    @staticmethod
    def invertBytes(byte_array):
        return [val^0xff for val in byte_array]

    def pack(self, bytes_list):
        out = [len(bytes_list)]
        out += bytes_list
        return out

    def resetMUCommunicationPolling(self, timeout=1):
        t_timeout = time.time()+timeout
        # write until mu response to [0] is COMM_OK
        # -> mu in command_phase
        # -> mu command buffer is ready for communication start
        while time.time() < t_timeout:
            if self.xfer([0])[0] == COMM_OK:
                return
        raise ResetError()

    def resetMUCommunicationMUAutoReset(self):
        comm_reset_wait_time = SpiControl.COMM_RESET_TIME*1.5
        # MU resets communication after timeout
        # -> wait the time specified in MU to ensure clean communication state
        t_wait = max(0, comm_reset_wait_time - (time.time()-self.last_xfer_time))
        time.sleep(t_wait)
        # ensure that command buffer is empty
        for i in range(SpiControl.COMM_COMMAND_BUFFER_SIZE):
            if self.xfer([0])[0] == COMM_OK:
                return
        raise ResetError()

    def resetMUCommunication(self, reset_mechanism="polling", timeout=None):
        if reset_mechanism == "polling":
            timeout_arg = timeout if timeout is not None else 1
            self.resetMUCommunicationPolling(timeout=timeout_arg)
        elif reset_mechanism == "mu_auto_reset":
            self.resetMUCommunicationMUAutoReset()

    def xfer(self, data_bytes, check_timing=False):
        response = self.spi.xfer(data_bytes)
        t_now = time.time()
        if check_timing:
            passed_xfer_time = t_now-self.last_xfer_time
            if passed_xfer_time > 0.95*SpiControl.COMM_RESET_TIME:
                raise RealtimeViolation()
        self.last_xfer_time = t_now
        return response

    def send(self, cmd, timeout=1):
        t_timeout = time.time()+timeout
        response = None
        errors = []
        while time.time()<t_timeout:
            try:
                self.resetMUCommunication()

                # command phase
                msg = self.pack(cmd)
                command_response = self.xfer(msg + [0])[1:]
                communication_ok = msg == SpiControl.invertBytes(command_response)
                if communication_ok:
                    self.xfer([COMM_OK], check_timing=True) # send ack
                else:
                    self.xfer([COMM_ERR]) # send nack
                    raise CommandTransError()

                # stretch phase
                t_response_timeout = time.time()+(10*SpiControl.COMM_BYTE_TRANSFER_TIME)
                while True:
                    stretch_response = self.xfer([COMM_DUMMY], check_timing=True)
                    if stretch_response == [COMM_RESPONSE_START]:
                        break
                    if not stretch_response == [COMM_RESPONSE_STRETCH]:
                        raise ResponseStretchError()
                    if time.time()>=t_response_timeout:
                        raise ResponseTimeout()
                    time.sleep(SpiControl.COMM_BYTE_TRANSFER_TIME)

                # response phase
                response = self.xfer([COMM_DUMMY], check_timing=True)
                response_length = response[0]
                if response_length >= COMM_OK:
                    raise Exception("{} error".format(response_length))
                for i in range(response_length+1):
                    # echo the last received byte inverted
                    response += self.xfer(SpiControl.invertBytes(response[-1:]), check_timing=True)
                response = response[:-1]

                response_ack = self.xfer([COMM_DUMMY], check_timing=True)
                if not response_ack == [COMM_OK]:
                    raise ResponseTransError()

                return response[1:],errors

            except SpiControlError as e:
                errors.append(e)

        raise SpiControlError(cmd, errors)

    def sendNOP(self):
        msg = [COMMAND_NOP]
        response,attempts = self.send(msg)
        return int(SpiControl.decode(response)) == COMM_RESPONSE_EMPTY

    def getCommandBufferSize(self):
        msg = [COMMAND_GET_COMMAND_BUFFER_SIZE]
        response,attempts = self.send(msg)
        return int(SpiControl.decode(response))

    def getResponseBufferSize(self):
        msg = [COMMAND_GET_RESPONSE_BUFFER_SIZE]
        response,attempts = self.send(msg)
        return int(SpiControl.decode(response))

    def getVersion(self):
        msg = [COMMAND_GET_VERSION]
        response,attempts = self.send(msg)
        return int(SpiControl.decode(response))

    def getDebug(self, index):
        msg = [COMMAND_GET_DEBUG] + SpiControl.encode(index, 1)
        response,attempts = self.send(msg)
        return int(SpiControl.decode(response))
