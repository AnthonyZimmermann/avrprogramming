from avrcontrol.spicontrol import SpiControl

def test_getVersion():
    with SpiControl() as spicontrol:
        version = spicontrol.getVersion()
#        print("version:",version)

def test_getCommandBufferSize():
    with SpiControl() as spicontrol:
        command_buffer_size = spicontrol.getCommandBufferSize()
#        print("command_buffer_size:",command_buffer_size)

def test_getResponseBufferSize():
    with SpiControl() as spicontrol:
        response_buffer_size = spicontrol.getResponseBufferSize()
#        print("response_buffer_size:",response_buffer_size)

def test_sendNOP():
    with SpiControl() as spicontrol:
        assert spicontrol.sendNOP() == True
