from setuptools import setup

setup(name="avrcontrol",
      version="1.0",
      description="Installs avrcontrol, a template interface to communicate with the microcontroller",
      author="Anthony Zimmermann",
      author_email="anthony.zimmermann@protonmail.com",
      package=["avrcontrol"],
      scripts=[],
      include_package_data=True)
