#ifndef MAIN_H
#define MAIN_H

#define F_CPU 1000000UL

#include <stdbool.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <util/atomic.h>

#define ATOMIC_OP(op) ATOMIC_BLOCK(ATOMIC_FORCEON) { op; }

//#define DEBUG
#define VERSION 5
/*
 * Communication flow example: command 'get version'
 * row #1: valuable tx information
 * row #2: valuable rx information
 * +--------------+--------------+---------------------------------------------+
 * |   master     |   mu (slave) |   comment                                   |
 * +--------------+--------------+---------------------------------------------+
 * | 1            |              | master sends # of following command bytes   |
 * |              | 1            | #command_bytes = command_code(1) + #data    |
 * +--------------+--------------+---------------------------------------------+
 * | COMM_GET_VER | 1            | master sends command_code; mu echos         |
 * | 1            | COMM_GET_VER | #command_bytes                              |
 * +--------------+--------------+---------------------------------------------+
 * |              | COMM_GET_VER | mu echos command_code                       |
 * | COMM_GET_VER |              |                                             |
 * +--------------+--------------+---------------------------------------------+
 * | COMM_OK      |              | master acknowledges correctness of echoed   |
 * |              | COMM_OK      | bytes: command_code + data                  |
 * +--------------+--------------+---------------------------------------------+
 * |              | RESP_STRETCH | mu sends a stretch byte to tell master that |
 * | RESP_STRETCH |              | it is searching for the answer/response     |
 * +--------------+--------------+---------------------------------------------+
 * |              | RESP_STRETCH | mu sends a stretch byte to tell master that |
 * | RESP_STRETCH |              | it is searching for the answer/response     |
 * +--------------+--------------+---------------------------------------------+
 * |              | RESP_START   | mu sends a ready byte to tell master that   |
 * | RESP_START   |              | it will start with the response now         |
 * +--------------+--------------+---------------------------------------------+
 * |              | 1            | mu sends # of following response bytes (1)  |
 * | 1            |              | #resp_bytes = #data                         |
 * +--------------+--------------+---------------------------------------------+
 * | 1            | 0xAB         | mu sends data bytes starting with the first |
 * | 0xAB         | 1            | byte; master echos #response_bytes          |
 * +--------------+--------------+---------------------------------------------+
 * | 0xAB         |              | master echos data bytes starting with the   |
 * |              | 0xAB         | first data byte                             |
 * +--------------+--------------+---------------------------------------------+
 * |              | COMM_OK      | mu acknowledges correctness of echoed       |
 * | COMM_OK      |              | bytes: data bytes                           |
 * +--------------+--------------+---------------------------------------------+
 */

#ifdef DEBUG
#define DEBUG_COMMAND
#define NUM_DEBUG_VARIABLES 16
#endif

#define COMMAND_BUFFER_SIZE             16
#define RESPONSE_BUFFER_SIZE            16

#define SPI_DDR_INIT                    DDRB = (DDRB & 0x1e) | (1<<PB4)
#define SPI_INIT                        SPCR = (1<<SPE)
#define SPI_INTERRUPT_ENABLE            SPCR |= (1<<SPIE)
#define SPI_INTERRUPT_DISABLE           SPCR &= ~(1<<SPIE)

#define DATA_TX_ON                      DDRB |= (1<<PB1)  // OC1A pin
#define DATA_TX_OFF                     DDRB &= ~(1<<PB1) // OC1A pin

/*
 * SETTINGS FOR COMMUNICATION BUFFER SIZES
 * BUFFER_SIZEs max allowed value: 250
 * -> higher values cause a uint8 overflow at comparisons
 */
// why +3: overhead bytes, rest is max comunication bytes
// for further information, see table above
#define COMM_COMMAND_BUFFER_SIZE        COMMAND_BUFFER_SIZE+3
#define COMM_RESPONSE_BUFFER_SIZE       RESPONSE_BUFFER_SIZE+3
// this time must be set according to the transfer speed
#define COMM_RESET_DEFER_TIMEOUT_US     10000

// STRETCH must be different from COMM_RESPONSE_START
#define COMM_RESPONSE_STRETCH           0xF9 // 249
// START must be different from COMM_RESPONSE_STRETCH
#define COMM_RESPONSE_START             0xFE // 254
#define COMM_OK                         0xF0 // 240
#define COMM_ERR                        0xF3 // 243
#define COMM_RESPONSE_EMPTY             0xFA // 250

// NOP: short dummy command can be used to test if the mu is ready for communication
//      -> mu should response with COMM_OK
#define COMMAND_NOP                         251
#define COMMAND_GET_COMMAND_BUFFER_SIZE     252
#define COMMAND_GET_RESPONSE_BUFFER_SIZE    253
#define COMMAND_GET_VERSION                 254

#ifdef DEBUG_COMMAND
#define COMMAND_GET_DEBUG                   255 // debug
#endif

#define COMMAND_SET_DUMMY_VALUE             0
#define COMMAND_GET_DUMMY_VALUE             1

volatile uint8_t comm_command_buffer[COMM_COMMAND_BUFFER_SIZE];
volatile uint8_t comm_command_buffer_pointer;
volatile uint8_t comm_command_buffer_ack_pointer;
volatile uint8_t comm_response_buffer[COMM_RESPONSE_BUFFER_SIZE];
volatile uint8_t comm_response_buffer_pointer;
volatile uint8_t comm_response_buffer_prepare_pointer;
volatile uint8_t comm_response_buffer_ack_pointer;

volatile bool comm_command_phase;
volatile bool comm_stretch_phase;
volatile bool comm_response_check_ok;
volatile bool comm_reset_defer;
volatile bool comm_reset_force;

uint16_t dummy_value;

#ifdef DEBUG_COMMAND
uint64_t debug_values[NUM_DEBUG_VARIABLES];
#endif

// process communication input
int main(void);

void spi_init();

void init_comm();
void comm_reset();

void comm_stretch();
bool comm_command_received();
void comm_append_response_byte(uint8_t);
void comm_send_response();

uint16_t comm_command_buffer_read_uint16(uint8_t pos);
uint32_t comm_command_buffer_read_uint32(uint8_t pos);
uint64_t comm_command_buffer_read_uint64(uint8_t pos);

void comm_append_response_uint16(uint16_t value);
void comm_append_response_uint32(uint32_t value);
void comm_append_response_uint64(uint64_t value);

void process_comm_commands();

#endif
