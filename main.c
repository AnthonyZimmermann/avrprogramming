#include "main.h"

inline void comm_command_receiption() __attribute__((always_inline));
inline void comm_response() __attribute__((always_inline));

int main(void)
{
    init_comm();
    spi_init();

    sei();

#ifdef DEBUG_COMMAND
    uint8_t i;
    for (i=0; i<DEBUG_NUM_VARIABLES; i++)
    {
        debug_values[i] = 0;
    }
#endif

    while(1)
    {
        process_comm_commands();
    }
}

// LOW LEVEL BUFFERED COMMUNICATION INTERFACE SPI //

void spi_init()
{
    SPI_DDR_INIT;
    SPI_INIT;
    SPI_INTERRUPT_ENABLE;
}

ISR(SPI_STC_vect)
{
    if (comm_command_phase)
        comm_command_receiption();
    else
    {
        ATOMIC_OP(comm_reset_defer = 1);
        if (comm_stretch_phase)
            SPDR = COMM_RESPONSE_STRETCH;
        else
            comm_response();
    }
}

inline void comm_command_receiption()
{
    ATOMIC_OP(comm_command_buffer[comm_command_buffer_pointer] = SPDR);

    if (comm_command_buffer_ack_pointer == 0)
    {
        // here comm_command_buffer_pointer is always '0'
        // -> if comm_command_buffer_ack_pointer is not '0' here,
        // command code (2nd byte) has already been written into
        // comm_command_buffer
        const uint8_t comm_length = comm_command_buffer[0];

        if (comm_length > COMMAND_BUFFER_SIZE)
        {
            ATOMIC_OP(comm_command_buffer_pointer = 0);
            SPDR = COMM_ERR;
            return;
        }


        if (comm_length == 0)
        {
            ATOMIC_OP(comm_command_buffer_pointer = 0);
            SPDR = COMM_OK;
            return;
        }

        // why +2:
        // comm_length is the first byte in comm_command_buffer
        // and stores the number of following command bytes -> +1
        // one dummy byte (last byte) has been stored in comm_command_buffer
        // that is no valuable information, but 'we' needed one byte transfer
        // to echo the last received command byte to master -> +1
        ATOMIC_OP(comm_command_buffer_ack_pointer = comm_length+2);
    }

    if (comm_command_buffer_pointer == comm_command_buffer_ack_pointer)
    {
        ATOMIC_BLOCK(ATOMIC_FORCEON)
        {
            comm_command_phase = false;
            //comm_command_buffer_pointer = 0;
            comm_reset_defer = 1;
        }
        SPDR = COMM_RESPONSE_STRETCH;
        return;
    }

    // send inverted echo when byte gets added to command buffer
    SPDR = SPDR^0xff;
    ATOMIC_OP(comm_command_buffer_pointer++);
}

inline void comm_response()
{
    if (comm_response_buffer_pointer == 255)
    {
        SPDR = COMM_RESPONSE_START;
        ATOMIC_OP(comm_response_buffer_pointer = 0);
        return;
    }

    // why >= 2:
    // Master initiates the communication and echos what slave says
    // Slave can evaluate the echo earliest two byte-cycles later
    if (comm_response_buffer_pointer>=2)
    {
        // why -2:
        // Master initiates the communication and echos inverted what slave says
        // Slave can evaluate the echo earliest two byte-cycles later
        const uint8_t master_echo_byte = SPDR^0xff;
        if (master_echo_byte != comm_response_buffer[comm_response_buffer_pointer-2])
        {
            ATOMIC_OP(comm_response_check_ok = false);
        }
    }

    if (comm_response_buffer_pointer == comm_response_buffer_ack_pointer)
    {
        SPDR = comm_response_check_ok ? COMM_OK : COMM_ERR;
        ATOMIC_OP(comm_reset_force = 1); // reset communication as fast as possible
        return;
    }

    if (comm_response_buffer_pointer < COMM_RESPONSE_BUFFER_SIZE)
    {
        SPDR = comm_response_buffer[comm_response_buffer_pointer];
        ATOMIC_OP(comm_response_buffer_pointer++);
    }
}


// HIGH LEVEL COMMUNICATION INTERFACE //

void init_comm()
{
    comm_reset();
}

void comm_reset()
{
    if (comm_stretch_phase)
    {
        comm_append_response_byte(COMM_RESPONSE_EMPTY);
        comm_send_response();
    }
    // asynchronous defer timeout
    // During _delay_us(), ISR can set comm_reset_defer to '1'
    uint8_t i;
    while (comm_reset_defer)
    {
        ATOMIC_OP(comm_reset_defer = 0);

        // poll (1<<division_shift) times:
        // should we reset the communication immediately
        const uint8_t division_shift = 4;
        for (i=0; i<(1<<division_shift); i++)
        {
            _delay_us(COMM_RESET_DEFER_TIMEOUT_US>>division_shift);
            if (comm_reset_force)
                goto L_COMM_RESET_CONTINUE;
        }
    }

L_COMM_RESET_CONTINUE:

    ATOMIC_BLOCK(ATOMIC_FORCEON)
    {
        comm_command_buffer[0] = 0;
        comm_command_buffer_pointer = 0;
        comm_command_buffer_ack_pointer = 0;

        comm_response_buffer_pointer = 254;
        comm_response_buffer_ack_pointer = 254;
        comm_response_buffer_prepare_pointer = 0;

        comm_reset_force = 0;
        comm_response_check_ok = true;
        comm_stretch_phase = true;
        comm_command_phase = true;
    }
}

void process_comm_commands()
{
    if (comm_command_received())
    {
        switch(comm_command_buffer[1])
        {
            case COMMAND_SET_DUMMY_VALUE:
                dummy_value = comm_command_buffer_read_uint16(2);
                // break; -> automaticly respond with new configuration
            case COMMAND_GET_DUMMY_VALUE:
                comm_append_response_uint16(dummy_value);
                comm_send_response();
                break;

            case COMMAND_NOP:
                break;
            case COMMAND_GET_COMMAND_BUFFER_SIZE:
                comm_append_response_byte(COMMAND_BUFFER_SIZE);
                comm_send_response();
                break;
            case COMMAND_GET_RESPONSE_BUFFER_SIZE:
                comm_append_response_byte(RESPONSE_BUFFER_SIZE);
                comm_send_response();
                break;
            case COMMAND_GET_VERSION:
                comm_append_response_byte(VERSION);
                comm_send_response();
                break;
            default:
                break;
        }

        comm_reset(); // always reset communication
L_PROCESS_COMM_COMMANDS_NO_RESET:
        return;
    }
}

bool comm_command_received()
{
    if (comm_command_phase)
        return false;

    // why +2:
    // first byte determinse length of command
    // appended dummy byte to echo last received byte back to master for checking
    if (comm_command_buffer[comm_command_buffer_ack_pointer] == COMM_OK)
        return true;

    comm_reset();
    return false;
}

void comm_append_response_byte(uint8_t byte)
{
    // one byte must be reserved for a flush byte
    if (comm_response_buffer_prepare_pointer < RESPONSE_BUFFER_SIZE-1)
    {
        ATOMIC_OP(comm_response_buffer[++comm_response_buffer_prepare_pointer] = byte);
    }
}

void comm_send_response()
{
    if (comm_response_buffer_prepare_pointer > 0)
    {
        ATOMIC_BLOCK(ATOMIC_FORCEON)
        {
            // number of bytes in response buffer
            comm_response_buffer[0] = comm_response_buffer_prepare_pointer;
            // why +2:
            // Master initiates the communication and echos what slave says
            // Slave can evaluate the echo earliest two byte-cycles later
            comm_response_buffer_pointer = 255;
            comm_response_buffer_ack_pointer = comm_response_buffer_prepare_pointer+2;
            //comm_response_buffer[comm_response_buffer_prepare_pointer+1] = 0x00;
            comm_stretch_phase = false;
        }
    }
}

uint16_t comm_command_buffer_read_uint16(uint8_t pos)
{
    uint16_t ret;
    uint8_t i;
    for (ret=0,i=pos+1; i>=pos; i--) // read msb first, start at highest index
    {
        ret = (ret<<8) | comm_command_buffer[i];
    }
    return ret;
}

uint32_t comm_command_buffer_read_uint32(uint8_t pos)
{
    uint32_t ret;
    uint8_t i;
    for (ret=0,i=pos+3; i>=pos; i--) // read msb first, start at highest index
    {
        ret = (ret<<8) | comm_command_buffer[i];
    }
    return ret;
}

uint64_t comm_command_buffer_read_uint64(uint8_t pos)
{
    uint64_t ret;
    uint8_t i;
    for (ret=0,i=pos+7; i>=pos; i--) // read msb first, start at highest index
    {
        ret = (ret<<8) | comm_command_buffer[i];
    }
    return ret;
}

void comm_append_response_uint16(uint16_t value)
{
    uint8_t i;
    for (i=0; i<2; i++)
    {
        comm_append_response_byte(value&0xFF);
        value >>= 8;
    }
}

void comm_append_response_uint32(uint32_t value)
{
    uint8_t i;
    for (i=0; i<4; i++)
    {
        comm_append_response_byte(value&0xFF);
        value >>= 8;
    }
}

void comm_append_response_uint64(uint64_t value)
{
    uint8_t i;
    for (i=0; i<8; i++)
    {
        comm_append_response_byte(value&0xFF);
        value >>= 8;
    }
}
